require("dotenv").config();
const express = require("express");
const app = express();

const PORT = process.env.PORT || 3000;

process.env.DEBUG === "true" ? process.env.DEBUG = process.env.APP_NAME + ":*": process.env.DEBUG = "";
const debug = require("debug")(`${process.env.APP_NAME}:startup`);

const { templateEngineMiddleware, initGlobalVariables, securityMiddleware, cookieMiddleware } = require("./middleware/startup");
const router = require("./middleware/routes");

debug(`running in ${process.env.ENVIRONMENT}`);
app.use(securityMiddleware);
debug("security middleware");
app.use(cookieMiddleware);
debug("persisting cookie");
app.use(initGlobalVariables);
debug("initalizing global variables");
app.use(express.static("public"));
debug("setting static middleware");
app.use(templateEngineMiddleware);
debug("initalizing template engines");
app.use(router);
debug("creating routes");

app.listen(PORT, () => {
    debug(`app listening on port ${PORT}`);
});

module.exports = app;