const exphbs = require("express-handlebars");
const helmet = require("helmet");
const cookieSession = require("cookie-session");

const templateEngineMiddleware = (req, res, next) => {
    req.app.engine(".hbs", exphbs({
        extname: ".hbs"
    }));
    req.app.set("view engine", ".hbs");
    next();
}

const securityMiddleware = (req, res, next) => {
    req.app.use(helmet());
    next();
}

const initGlobalVariables = (req, res, next) => {
    req.app.use((req, res, next) => {
        res.locals.helpers = require("../helpers/hbs");
        next();
    });
    next();
}

const cookieMiddleware = (req, res, next) => {
    req.app.use(cookieSession({
        name: "session",
        keys: ["key1", "key2"]
    }));
    next();
}




module.exports = {
    templateEngineMiddleware,
    initGlobalVariables,
    securityMiddleware,
    cookieMiddleware
}