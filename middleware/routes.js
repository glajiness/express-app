module.exports = (req, res, next) =>{
    req.app.use("/", require("../views/pages/index/indexRouter"));
    next();
}