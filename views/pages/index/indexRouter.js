const router = require("express").Router();

//controllers
const { getIndex } = require("./indexController");

//routes relative to /
router.get("/", getIndex, (req, res, next) => {
    res.render("pages/index", { helloWorld: res.helloWorld });
});

module.exports = router;