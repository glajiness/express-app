//services
const helloWorld = require("../../../services/helloworld/index");

const getIndex = (req, res, next) => {
    res.helloWorld = helloWorld;
    next();
}


module.exports = {
    getIndex
}