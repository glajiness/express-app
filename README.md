# EXPRESS APP BOILERPLATE

This project is a starting place for an express app. This project has the following dependecies.

* [Express](https://github.com/expressjs/express)
* [Mongoose](https://github.com/Automattic/mongoose)
* [Express Handlebars](https://github.com/ericf/express-handlebars)
* [Cookie Session](https://github.com/expressjs/cookie-session)
* [Debug](https://github.com/visionmedia/debug)
* [DotEnv](https://github.com/motdotla/dotenv)
* [Helmet](https://github.com/helmetjs/helmet)

## Running application

```shell
    npm start
```

## Debugging application

```shell
    npm run debug
```

## Middleware

This is the middleware that makes up the request/response pipeline. 

### Startup Middleware


* template engine middleware - initalizes [handlebars](https://handlebarsjs.com/) in the project.
* security middleware - initalizes [helmet](https://helmetjs.github.io/) in the project.
* global variables - initializes [global variables](https://expressjs.com/en/api.html#app.locals) to be uses throughout the project.
* cookie middleware - sets cookie in project with [cookie session](https://github.com/expressjs/cookie-session#readme)

### Routes middleware

Global routes that initialize the global namespace.

## Views

Views should hold the logic for the specific pages in our application and should have the following items.

### hbs files

* HTML templates that have our html.

### Router

* The router defines the routes relative to the global namespace.

### Controller

* The controller controls the logic of the specific route to avoid over complicating the routes.

## Services

A service is something the application that can use that should not be dependent on express. The service is typically share among views, routes, and middleware. The idea behind these services is that they can later be independent from the application and be there own microservices.

